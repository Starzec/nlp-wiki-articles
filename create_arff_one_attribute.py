from constants import classes

if __name__ == '__main__':
    file = open('data_set_one_attribute.arff', 'w')
    file.write('@relation nlp-wiki-words\n\n')
    file.write('@attribute text STRING\n')
    file.write('@attribute classLabel {' + str(','.join([c[0] for c in classes])) + '}\n\n')
    file.write('@data\n')

    for class_name, number in classes:
        for i in range(1, number + 1):
            class_file = open(f'Preprocessed-data/{class_name}/{class_name}_{i}.txt', 'r')
            file.write('\"' + class_file.read() + '\"' + f',{class_name}\n')
            class_file.close()
    file.close()
