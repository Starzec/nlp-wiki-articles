import json

from constants import classes


def go_via_file(words: dict, filename: str, count=300):
    for i in range(1, count + 1):
        file = open(f'Preprocessed-data/{filename}/{filename}_{i}.txt', 'r')
        text = file.read()
        all_words = text.split()
        for word in all_words:
            if word not in words:
                words[word] = 1
            else:
                words[word] = words[word] + 1
        file.close()


if __name__ == '__main__':
    all_words_dict = {}
    for c, number in classes:
        go_via_file(all_words_dict, c, number)
    f = open('words.json', 'w')
    json.dump(all_words_dict, f, ensure_ascii=False)
    f.close()
