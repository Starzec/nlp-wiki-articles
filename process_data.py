import re

from constants import classes


def process_data(filename: str, count=300):
    for i in range(1, count + 1):
        file = open(f'Source-data/{filename}/{filename}_{i}.txt', 'r')
        s = file.read().lower()
        file.close()
        res = re.findall(re.compile(r"(?:(?![\d_])\w)+", re.UNICODE), s)
        file = open(f'Preprocessed-data/{filename}/{filename}_{i}.txt', 'w')
        file.write(' '.join(res))
        file.close()


if __name__ == '__main__':
    for c, number in classes:
        process_data(c, number)
