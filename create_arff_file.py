import json

from constants import classes


def write_whole_class(f, words, class_name, number_of_files, last_index):
    for i in range(1, number_of_files + 1):
        class_file = open(f'Preprocessed-data/{class_name}/{class_name}_{i}.txt', 'r')
        text_list = class_file.read().split()
        class_file.close()
        f.write('{')
        for index, w in enumerate(words):
            if w in text_list:
                f.write(str(index) + ' 1,')
        f.write(str(last_index) + ' ' + class_name + '}\n')


if __name__ == '__main__':
    file = open('words.json', 'r')
    all_unique_words = list(json.load(file).keys())
    file.close()

    number_of_all_words = len(all_unique_words)
    file = open("data_set.arff", 'w')
    file.write('@relation nlp-wiki-words\n\n')
    for word in all_unique_words:
        file.write(f'@attribute {word} NUMERIC\n')
    file.write('@attribute classLabel {' + str(','.join([c[0] for c in classes])) + '}\n\n')
    file.write('@data\n')
    for c, number in classes:
        write_whole_class(file, all_unique_words, c, number, number_of_all_words)
        print(c)
    file.close()
